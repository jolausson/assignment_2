#!/bin/bash
all: certvalidator.c
	gcc -I /usr/local/opt/openssl/include -L /usr/local/opt/openssl/lib -o certvalidator certvalidator.c -lssl -lcrypto
clean:
	rm -rf certvalidator
